import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';

export const ratesApi = createApi({
  reducerPath: 'ratesApi',
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://api.fxratesapi.com/',
  }),

  endpoints: builder => ({
    getRates: builder.query({
      query: (baseCurrency: string) => {
        return {
          url: `latest?base=${baseCurrency}&resolution=1m&amount=1&places=6&format=json`,
          method: 'GET',
        };
      },
    }),
  }),
});

export const {useLazyGetRatesQuery} = ratesApi;

export const {endpoints, reducerPath, reducer} = ratesApi;
