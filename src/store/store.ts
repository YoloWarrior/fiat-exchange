import {combineReducers, configureStore} from '@reduxjs/toolkit';
import currencyReducer from './reducers/CurrencySlice';
import {ratesApi} from '@services/RatesService';

const rootReducer = combineReducers({
  currencyReducer,
  [ratesApi.reducerPath]: ratesApi.reducer,
});

export const setupStore = () => {
  return configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware =>
      getDefaultMiddleware({serializableCheck: false}).concat(
        ratesApi.middleware,
      ),
  });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore['dispatch'];
