import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {Currency} from '@interfaces/Currency';

interface CurrencyState {
  currencies: Currency[];
  currencyFrom?: Currency;
  currencyTo?: Currency;
}

const initialState: CurrencyState = {
  currencies: [],
  currencyFrom: undefined,
  currencyTo: undefined,
};

const currencySlice = createSlice({
  name: 'currency',
  initialState,
  reducers: {
    setCurrencies: (state, action: PayloadAction<Currency[]>) => {
      state.currencies = action.payload;
    },
    setCurrencyFrom: (state, action: PayloadAction<Currency>) => {
      state.currencyFrom = action.payload;
    },
    setCurrencyTo: (state, action: PayloadAction<Currency>) => {
      state.currencyTo = action.payload;
    },
    switchSelectedCurrencies: state => {
      let currecyFromState = state.currencyFrom;
      let currencyToState = state.currencyTo;

      state.currencyTo = currecyFromState;
      state.currencyFrom = currencyToState;
    },
  },
});

export const {
  setCurrencies,
  setCurrencyFrom,
  setCurrencyTo,
  switchSelectedCurrencies,
} = currencySlice.actions;

export default currencySlice.reducer;
