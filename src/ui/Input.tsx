import React from 'react';
import {KeyboardTypeOptions, StyleSheet, TextInput, View} from 'react-native';

interface InputProps {
  onChange?: (text: string) => void;
  value: string;
  placeholder?: string;
  icon?: React.ReactNode;
  keyboardType?: KeyboardTypeOptions;
}

const Input = ({
  value,
  placeholder,
  icon,
  keyboardType = 'default',
  onChange,
}: InputProps) => {
  return (
    <View style={styles.container}>
      {icon && icon}
      <TextInput
        onChangeText={onChange}
        value={value}
        keyboardType={keyboardType}
        placeholder={placeholder}
        style={[styles.input]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 43,
    borderRadius: 8,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingLeft: 16,
    gap: 8,
  },
  input: {
    flex: 1,
  },
});

export default Input;
