import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';

interface RadioProps {
  isSelected: boolean;
  selectable?: boolean;
  onSelectionChange?: (isSelected: boolean) => void;
}

const Radio = ({
  isSelected: defaultValue = false,
  selectable = true,
  onSelectionChange,
}: RadioProps) => {
  return (
    <TouchableOpacity
      style={styles.container}
      disabled={!selectable}
      onPress={() => {
        onSelectionChange && onSelectionChange(!defaultValue);
      }}>
      {defaultValue && <View style={styles.selectedRadio} />}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 16,
    width: 16,
    borderRadius: 27,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedRadio: {
    height: 8,
    width: 8,
    borderRadius: 21,
    backgroundColor: 'black',
  },
});

export default Radio;
