import {useNavigation, useRoute} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useAppDispatch, useAppSelector} from '@hooks/useRedux';
import Input from '@ui/Input';
import {Currency} from '@interfaces/Currency';
import {setCurrencyFrom, setCurrencyTo} from '@store/reducers/CurrencySlice';
import CurrencyItem from '@components/CurrencyItem';
import ArrowLeft from '@assets/icons/arrow-left.svg';
import Search from '@assets/icons/search.svg';

const CurrencyList = () => {
  const {selectType} = useRoute().params as any;
  const {currencies, currencyFrom, currencyTo} = useAppSelector(
    state => state.currencyReducer,
  );

  const dispatch = useAppDispatch();
  const navigation = useNavigation();

  const [searchText, setSearchText] = useState('');
  const [filteredCurrencies, setFilteredCurrencies] = useState(currencies);

  useEffect(() => {
    setFilteredCurrencies(
      currencies.filter(currency =>
        currency.code.toLowerCase().includes(searchText.toLowerCase()),
      ),
    );
  }, [searchText, currencies]);

  const handleSelectionChange = (currency: Currency) => {
    dispatch(
      selectType === 'from'
        ? setCurrencyFrom(currency)
        : setCurrencyTo(currency),
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.modalBackContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <ArrowLeft />
        </TouchableOpacity>

        <Text style={styles.modalBackText}>Currency Select</Text>
      </View>
      <Input
        value={searchText}
        onChange={setSearchText}
        placeholder="Search by code"
        icon={<Search />}
      />
      <FlatList
        style={styles.scrollContainer}
        data={filteredCurrencies}
        contentContainerStyle={styles.scrollItem}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item.code}
        renderItem={({item}) => (
          <CurrencyItem
            {...item}
            isSelected={
              item.code ===
              (selectType === 'from' ? currencyFrom?.code : currencyTo?.code)
            }
            onPress={() => handleSelectionChange(item)}
          />
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: Platform.OS === 'ios' ? 32 : 42,
    paddingBottom: 20,
  },
  modalBackContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 12,
    marginBottom: 16,
  },
  modalBackText: {
    fontSize: 20,
    fontWeight: '700',
  },
  scrollContainer: {
    marginTop: 16,
  },
  scrollItem: {
    gap: 8,
  },
});

export default CurrencyList;
