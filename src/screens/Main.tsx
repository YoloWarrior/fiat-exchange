/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useLazyGetRatesQuery} from '@services/RatesService';
import currencies from '@assets/currencies.json';
import {Currency} from '@interfaces/Currency';
import {useAppDispatch, useAppSelector} from '@hooks/useRedux';
import NetInfo from '@react-native-community/netinfo';
import {
  setCurrencies,
  setCurrencyFrom,
  setCurrencyTo,
} from '@store/reducers/CurrencySlice';
import Input from '@ui/Input';
import ExchangeCalculator from '@components/ExchangeCalculator';
import CurrencySelect from '@components/CurrencySelect';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Main = () => {
  const [exchangeValue, setExchangeValue] = useState('');
  const [getRates] = useLazyGetRatesQuery();
  const dispatch = useAppDispatch();
  const {currencyFrom, currencyTo} = useAppSelector(
    state => state.currencyReducer,
  );

  useEffect(() => {
    const fetchRates = async () => {
      const isConnected = (await NetInfo.fetch()).isConnected;
      let currenciesWithRates: Currency[] = [];

      if (isConnected) {
        const {isSuccess, data} = await getRates(
          currencyFrom ? currencyFrom.code : 'USD',
        );

        if (isSuccess) {
          currenciesWithRates = (currencies as Currency[]).map(currency => ({
            ...currency,
            rate: +data?.rates[currency.code],
          }));

          AsyncStorage.setItem(
            'currencies',
            JSON.stringify(currenciesWithRates),
          );
        }
      } else {
        const storedCurrencies = await AsyncStorage.getItem('currencies');
        if (storedCurrencies) {
          currenciesWithRates = JSON.parse(storedCurrencies);
        }
      }

      const defaultCurrencyValue = currenciesWithRates.find(
        currency => currency.code === 'USD',
      ) as Currency;

      let currencyFromValue =
        currencyFrom ||
        JSON.parse((await AsyncStorage.getItem('currencyFrom')) || 'null');

      let currencyToValue =
        (currencyTo && {
          ...currencyTo,
          rate: currenciesWithRates?.find(({code}) => code === currencyTo?.code)
            ?.rate,
        }) ||
        JSON.parse((await AsyncStorage.getItem('currencyTo')) || 'null') ||
        defaultCurrencyValue;

      dispatch(setCurrencies(currenciesWithRates));
      dispatch(setCurrencyFrom(currencyFromValue));
      dispatch(setCurrencyTo(currencyToValue));
    };

    fetchRates();
  }, [currencyFrom]);

  useEffect(() => {
    currencyFrom &&
      AsyncStorage.setItem('currencyFrom', JSON.stringify(currencyFrom));
    currencyTo &&
      AsyncStorage.setItem('currencyTo', JSON.stringify(currencyTo));
  }, [currencyFrom, currencyTo]);

  return (
    <View style={styles.container}>
      <CurrencySelect />

      <View style={styles.inputContainer}>
        <Text>Amount:</Text>
        <Input
          value={exchangeValue}
          onChange={setExchangeValue}
          keyboardType="numeric"
          placeholder={currencyFrom?.code}
        />
      </View>

      {currencyTo && (
        <ExchangeCalculator
          from={currencyFrom}
          to={currencyTo}
          value={+exchangeValue}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: 'center',
    backgroundColor: '#F5F5F5',
  },
  inputContainer: {
    gap: 8,
    marginTop: 16,
    marginBottom: 24,
  },
});

export default Main;
