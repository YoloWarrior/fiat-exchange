import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import CurrencySelectItem from '@components/CurrencySelectItem';
import {useAppDispatch, useAppSelector} from '@hooks/useRedux';
import ArrowLeftRight from '@assets/icons/arrow-left-right.svg';
import {NavigationProp, useNavigation} from '@react-navigation/native';
import {Screen} from '@navigation/types/Screens';
import {RootStackParamList} from '@navigation/Navigator';
import {switchSelectedCurrencies} from 'src/store/reducers/CurrencySlice';

const CurrencySelect = () => {
  const {currencyFrom, currencyTo} = useAppSelector(
    state => state.currencyReducer,
  );

  const navigation = useNavigation<NavigationProp<RootStackParamList>>();
  const dispatch = useAppDispatch();

  const handlePress = (selectType: string) => {
    navigation.navigate(Screen.Currencies, {selectType});
  };

  const handleSwitchCurrencies = () => dispatch(switchSelectedCurrencies());

  return (
    <View style={styles.conatainer}>
      <View style={styles.selectedCurrency}>
        <Text>From:</Text>
        <CurrencySelectItem
          text={currencyFrom?.code ?? ''}
          icon={currencyFrom?.flagSrc ?? ''}
          onPress={() => handlePress('from')}
        />
      </View>

      <TouchableOpacity
        onPress={handleSwitchCurrencies}
        style={styles.switchIcon}>
        <ArrowLeftRight />
      </TouchableOpacity>

      <View style={styles.selectedCurrency}>
        <Text>To:</Text>

        <CurrencySelectItem
          text={currencyTo?.code ?? ''}
          icon={currencyTo?.flagSrc ?? ''}
          onPress={() => handlePress('to')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  conatainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  selectedCurrency: {
    gap: 8,
  },
  switchIcon: {
    marginBottom: 12,
  },
});

export default CurrencySelect;
