import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Currency} from '@interfaces/Currency';

interface ExchangeCalculatorProps {
  from?: Currency;
  to?: Currency;
  value: number;
}

const ExchangeCalculator = ({from, to, value}: ExchangeCalculatorProps) => {
  return (
    <View>
      <Text>
        {value} {from?.symbol} =
      </Text>
      <Text style={styles.currencyToText}>
        {(value * (to?.rate ? to.rate : 0)).toFixed(2)} {to?.symbol}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  currencyToText: {
    fontSize: 42,
  },
});

export default ExchangeCalculator;
