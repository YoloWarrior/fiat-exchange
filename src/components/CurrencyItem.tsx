import React from 'react';
import {TouchableOpacity, Text, StyleSheet, Image, View} from 'react-native';
import Radio from '@ui/Radio';
import {Currency} from '@interfaces/Currency';

interface CurrencyItemProps extends Currency {
  isSelected: boolean;
  onPress: () => void;
}

const CurrencyItem = React.memo(
  ({code, name, isSelected, flagSrc, onPress}: CurrencyItemProps) => {
    const calculateStyles = () => {
      return {
        backgroundColor: isSelected ? '#DEDEDE' : '#FFFFFF',
      };
    };

    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.container, calculateStyles()]}>
        <View style={styles.info}>
          <Image src={flagSrc} style={styles.icon} />
          <Text>
            {code} - {name}
          </Text>
        </View>

        <Radio isSelected={isSelected} selectable={false} />
      </TouchableOpacity>
    );
  },
  (prevProps, nextProps) => {
    return prevProps.isSelected === nextProps.isSelected;
  },
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 18,
    borderRadius: 8,
  },
  info: {
    flexDirection: 'row',
    gap: 8,
    alignItems: 'center',
  },
  icon: {
    width: 30,
    height: 20,
  },
});

export default CurrencyItem;
