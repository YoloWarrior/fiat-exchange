import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import ArrowDown from '@assets/icons/arrow-down.svg';

interface CurrencySelectItemProps {
  text: string;
  icon: string;
  onPress: () => void;
}

const CurrencySelectItem = ({text, icon, onPress}: CurrencySelectItemProps) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      {icon && <Image style={styles.icon} src={icon} />}
      <Text>{text}</Text>
      <ArrowDown />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    gap: 8,
    backgroundColor: '#DEDEDE',
    borderRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  icon: {
    width: 30,
    height: 20,
  },
});

export default CurrencySelectItem;
