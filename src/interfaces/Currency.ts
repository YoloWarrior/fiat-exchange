export interface Currency {
  name: string;
  code: string;
  symbol: string;
  decimalDigits: number;
  flagSrc: string;
  rate?: number;
}
