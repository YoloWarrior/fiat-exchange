module.exports = {
  presets: ['module:@react-native/babel-preset'],
  plugins: [
    [
      'module:react-native-dotenv',
      {
        moduleName: '@env',
        path: '.env',
      },
    ],
    [
      'module-resolver',
      {
        root: ['.'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          '@src': './src',
          '@components': './src/components',
          '@assets': './src/assets',
          '@navigation': './src/navigation',
          '@services': './src/services',
          '@ui': './src/ui',
          '@screens': './src/screens',
          '@store': './src/store',
          '@hooks': './src/hooks',
          '@interfaces': './src/interfaces',
        },
      },
    ],
  ],
};
